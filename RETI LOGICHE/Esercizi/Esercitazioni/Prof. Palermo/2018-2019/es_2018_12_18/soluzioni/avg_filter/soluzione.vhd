library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity filter is
    Port ( I : in  STD_LOGIC_VECTOR (31 downto 0);
           clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           Y : out  STD_LOGIC_VECTOR (31 downto 0));
end filter;

architecture structural of filter is
signal i_div4_1, i_div4_2, i_div4_3, i_div4_4, sum1, sum2:  STD_LOGIC_VECTOR (31 downto 0);

component esempio13 is
port( in1 : in std_logic_vector(0 to 31);
      clk, rst : in std_logic;
      out1 : out std_logic_vector(0 to 31)
);
end component;

component esempio3 is 
  port(
    in1, in2: in std_logic_vector(31 downto 0);
    out1 : out std_logic_vector(31 downto 0);
    ovf : out std_logic
  );
end component;


begin

i_div4_1 <= "00" & (I(31 downto 2));

reg1: esempio13
  port map(i_div4_1, clk, rst, i_div4_2);

reg2: esempio13
  port map(i_div4_2, clk, rst, i_div4_3);

reg3: esempio13
  port map(i_div4_3, clk, rst, i_div4_4);

add1: esempio3
  port map(i_div4_1, i_div4_2, sum1, open);

add2: esempio3
  port map(sum1, i_div4_3, sum2, open);

add3: esempio3
  port map(sum2, i_div4_4, Y, open);

end structural;
